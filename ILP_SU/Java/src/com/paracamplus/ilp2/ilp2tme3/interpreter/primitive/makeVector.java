package com.paracamplus.ilp2.ilp2tme3.interpreter.primitive;

import java.math.BigInteger;
import java.util.Vector;

import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;

public class makeVector extends BinaryPrimitive {

	public makeVector() {
		super("makeVector");
	}

	@Override
	public Object apply(Object arg1, Object arg2) throws EvaluationException {
		if (!(arg1 instanceof BigInteger)) {
            String msg = "Non numeric argument";
            throw new EvaluationException(msg);
		}
		Vector<Object> v = new Vector<Object>();
		for (int i=0; i< ((BigInteger) arg1).intValue(); i++) {
			v.add(arg2);
		}
		return v;
	}

	

}
