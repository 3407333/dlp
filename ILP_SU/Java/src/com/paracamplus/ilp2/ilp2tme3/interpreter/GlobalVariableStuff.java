package com.paracamplus.ilp2.ilp2tme3.interpreter;

import java.io.Writer;
import java.math.BigDecimal;

import com.paracamplus.ilp1.interpreter.interfaces.IGlobalVariableEnvironment;
import com.paracamplus.ilp1.interpreter.primitive.Newline;
import com.paracamplus.ilp1.interpreter.primitive.Print;
import com.paracamplus.ilp2.ilp2tme3.interpreter.primitive.Sinus;
import com.paracamplus.ilp2.ilp2tme3.interpreter.primitive.makeVector;
import com.paracamplus.ilp2.ilp2tme3.interpreter.primitive.vectorGet;
import com.paracamplus.ilp2.ilp2tme3.interpreter.primitive.vectorLength;

public class GlobalVariableStuff {
    public static void fillGlobalVariables (
            IGlobalVariableEnvironment env,
            Writer out ) {
        env.addGlobalVariableValue("pi", new BigDecimal("3.1415926535"));
        env.addGlobalVariableValue(new Print(out));
        env.addGlobalVariableValue(new Newline(out));
        env.addGlobalVariableValue(new Sinus());
        env.addGlobalVariableValue(new makeVector());
        env.addGlobalVariableValue(new vectorLength());
        env.addGlobalVariableValue(new vectorGet());
    }
}
