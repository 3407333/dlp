package com.paracamplus.ilp2.ilp2tme3.interpreter.primitive;

import java.math.BigInteger;
import java.util.Vector;

import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;

public class vectorGet extends BinaryPrimitive {

	public vectorGet() {
		super("vectorGet");
	}

	@Override
	public Object apply(Object arg1, Object arg2) throws EvaluationException {
		if (arg1 instanceof Vector && arg2 instanceof BigInteger) {
			Vector<?> v = (Vector<?>) arg1;
			return v.get(((BigInteger) arg2).intValue());
		}
        String msg = "Wrong arguments";
        throw new EvaluationException(msg);
	}

}
