package com.paracamplus.ilp2.ilp2tme3.interpreter.primitive;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp1.interpreter.primitive.UnaryPrimitive;

public class Sinus extends UnaryPrimitive {
    
    public Sinus() {
        super("sinus");
    }
    

	@Override
	public Object apply(Object value) throws EvaluationException {
		if (value instanceof BigInteger) {
			BigInteger bi = (BigInteger) value;
			int n = bi.intValue();
			Double sin = Math.sin(n);
			return BigDecimal.valueOf(sin);
		} else if (value instanceof BigDecimal) {
			BigDecimal bd = (BigDecimal) value;
			double n = bd.doubleValue();
			Double sin = Math.sin(n);
			return BigDecimal.valueOf(sin);
		} else {
            String msg = "Non numeric argument";
            throw new EvaluationException(msg);
        }
	}
    
}
