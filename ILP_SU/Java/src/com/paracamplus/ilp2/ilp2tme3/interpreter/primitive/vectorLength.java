package com.paracamplus.ilp2.ilp2tme3.interpreter.primitive;

import java.util.Vector;

import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp1.interpreter.primitive.UnaryPrimitive;

public class vectorLength extends UnaryPrimitive {

	public vectorLength() {
		super("vectorLength");
	}

	@Override
	public Object apply(Object v) throws EvaluationException {
		if (v instanceof Vector<?>)
			return ((Vector<?>) v).size();
        String msg = "Non vector argument";
        throw new EvaluationException(msg);
	}

}
