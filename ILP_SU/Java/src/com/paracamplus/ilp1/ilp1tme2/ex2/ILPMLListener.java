package com.paracamplus.ilp1.ilp1tme2.ex2;

import com.paracamplus.ilp1.interfaces.IASTfactory;

import antlr4.ILPMLgrammar1tme2Parser.ConstFloatContext;
import antlr4.ILPMLgrammar1tme2Parser.ConstIntegerContext;

public class ILPMLListener extends com.paracamplus.ilp1.ilp1tme2.ex1.ILPMLListener {
	
	private static int constCpt = 0;
	
	public ILPMLListener(IASTfactory factory) {
		super(factory);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void enterConstInteger(ConstIntegerContext ctx) {
		System.out.println("Nb const : " + ++constCpt);
	}
	
	@Override
	public void enterConstFloat(ConstFloatContext ctx) {
		System.out.println("Nb const : " + ++constCpt);
	}
	
	
}