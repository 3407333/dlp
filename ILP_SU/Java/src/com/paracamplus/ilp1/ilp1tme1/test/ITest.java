package com.paracamplus.ilp1.ilp1tme1.test;


import java.io.File;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.paracamplus.ilp1.interpreter.test.InterpreterRunner;
import com.paracamplus.ilp1.interpreter.test.InterpreterTest;

@RunWith(Parameterized.class)
public class ITest extends InterpreterTest {
	
    protected static String[] samplesDirName = { "SamplesTME1" }; 
    protected static String pattern = "t[1-9]";
    
	public ITest(File file) {
		super(file);
	}
	
    @Parameters(name = "{0}")
    public static Collection<File[]> data() throws Exception {
    	return InterpreterRunner.getFileList(samplesDirName, pattern);
    }
	
}
