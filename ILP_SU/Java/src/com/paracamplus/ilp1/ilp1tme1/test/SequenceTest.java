package com.paracamplus.ilp1.ilp1tme1.test;

import org.junit.Test;

import com.paracamplus.ilp1.ast.ASTboolean;
import com.paracamplus.ilp1.ast.ASTexpression;
import com.paracamplus.ilp1.ast.ASTinteger;
import com.paracamplus.ilp1.ilp1tme1.sequence.ASTsequence;
import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;

public class SequenceTest {

	@Test
	public void test() {
		ASTexpression tab[] = { new ASTboolean("true"), new ASTboolean("false"), new ASTinteger("4") };

		ASTsequence seq = new ASTsequence(tab);
		try {
			seq.getAllButLastInstructions();
		} catch (EvaluationException e) {
			e.printStackTrace();
		}

	}

}
